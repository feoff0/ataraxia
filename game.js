

var gRandomChallangeTypes = [ "Luck" , "Appeal", "Trust" , "Curiosity" ];
var gRandomChallangeTypesSynonyms = { "L": "Luck" , "A": "Appeal" , "T":"Trust" , "C":"Curiosity"};

// internal function to look up thru synonym names
function challange_name ( challange )
{
if (challange in gRandomChallangeTypesSynonyms)
	return gRandomChallangeTypesSynonyms[challange];
return challange;
}


var gResourceType = [ "Love" , "Mind" , "Will" , "Health" , "Wealth"  ];

var null_item = { "id":"null" , label:"Nothing" ,       type:"null" , challange:"" , bonus:0,	 	 pic: "" , use:"NOTHING", description: "NOTHING"  };


var gItems = [
	null_item,
	// items
	{ "id":"sunglass", label:"Sunglasses" , 	type:"common" , challange:"Appeal" , bonus:25,	 	 pic: "  " , use:"Put on your sunglasses...", description: " "  },
	{ "id":"binocular", label:"Binocular" , 	type:"common" , challange:"Curiosity" , bonus:25, 	 pic: "  " , use:"See through binocular...", description: " "  },
	{ "id":"hat", label:"Highlander Hat" , 	type:"common" , challange:"Trust" , bonus:25,	 	 pic: "  " , use:"Put on the hat...", description: " "  },
	{ "id":"luckydice", label:"Lucky Dice" , 	type:"common" , challange:"Luck" , bonus:25,	 	 pic: "  " , use:"Roll your lucky dice...", description: " "  },

	{ "id":"honey", label:"Honey Sample" , 	type:"common" , challange:"Luck" , bonus:25,	 	 pic: "http://agriculture.vic.gov.au/__data/assets/image/0003/178950/hculture_img1.jpg" , use:"Taste honey...", description: " "  },

	// magical items
	{ "id":"cat", label:"The Cat",					type:"magical" , challange:"Luck" , bonus:50,	 	 pic: "https://ih1.redbubble.net/image.4239180.4675/flat,1000x1000,075,f.jpg" , use:"The Spirit helps you.", description: " "  },
	{ "id":"bear", label:"The Bear",				type:"magical" , challange:"Trust" , bonus:50,	 	 pic: "http://www.doloresbaker.com/uploads/3/4/9/4/34947287/4503668.jpg" , use:"The Spirit helps you.", description: " "  },
	{ "id":"unicorn", label:"The Unicorn",				type:"magical" , challange:"Appeal" , bonus:50,	 	 pic: "http://orig15.deviantart.net/f60c/f/2014/068/8/0/patronus_by_penny_dragon-d79m9fn.jpg" , use:"The Spirit helps you.", description: " "  },
	{ "id":"owl", label:"The Owl"	,				type:"magical" , challange:"Curiosity" , bonus:50, 	 pic: "http://www.theplotbunny.com/wp-content/uploads/2016/12/owl-1024x717.jpg" , use:"The Spirit helps you.", description: " "  },

	{ "id":"cosmetic_honey", label:"Magical Cosmetic Honey" , 	type:"magical" , challange:"Appeal" , bonus:55,	 	 pic: "http://www.giant-mountains.info/data/akce/1169/magical-honey-tuesdaysin-wellness-hotel-svornost-in-harrachov.jpg" , use:"Taste honey...", description: " "  }


	// cards
	
];

var ShowItemCallback = null; // a callback of user interface to add new item to the view

var gMainCharacter = {
	Inventory: [], // "item"
	Resources : [] // "resource type": "value"
};

// returns item by id
function LookupItem ( itemid ) {
	for (var i = 0 ; i < gItems.length; i++) {
		if (gItems[i].id == itemid)
			return gItems[i];
	}
	return null;
}

// adds an item to the character. if it's a string - it is treated as id. item is looked up in items database
function GameAddItem( item ) {
	if (typeof item == "string" )
		item = LookupItem(item);
	if (item) {
		gMainCharacter.Inventory.push(item);
		if (ShowItemCallback)
			ShowItemCallback(item);
	} else {
		alert(item + " item not found!");
	}
}


// get character items by challange type
function GameGetCharacterItemsByChallange( challange ) {
	// TODO: make challange type alliases
	challange = challange_name();
	items = [];
	for (var i = 0 ; i < gMainCharacter.Inventory.length; i++) {
		if (gMainCharacter.Inventory[i].challange == challange)
			items.push(gMainCharacter.Inventory[i]);
	}
	return items;
}

// get character items by id
function GameCharacterHaveItem( item ) {
	for (var i = 0 ; i < gMainCharacter.Inventory.length; i++) {
		if (gMainCharacter.Inventory[i].id == item)
			return gMainCharacter.Inventory[i];
	}
	return null;
}

// get character items by challange type
function GameGetCharacterMaxBonusItem ( challange) {
	items = GameGetCharacterItemsByChallange(challange);
	item = null_item;
	for (var i = 0 ; i < items.length; i++) {
		if (!item) {
			item = items[0];
		}
		if (item[i].bonus > item.bonus) {
			item = item[i];
		}
	}
	return item;
}

var MaxBonus = GameGetCharacterMaxBonusItem;
var HaveItem = GameCharacterHaveItem;
var AddItem = GameAddItem;