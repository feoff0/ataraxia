// This module contains functions to generate and manage game level - a mini-map with routes and micro-quests

// various auxiallary functions
function getParameterByName(name) {
    var match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search);
    return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
}

function loadCSS(href) {
  var cssLink = $("<link>");
  $("head").append(cssLink); //IE hack: append before setting href
  cssLink.attr({
    rel:  "stylesheet",
    type: "text/css",
    href: href
  });
}

function endLoading() {
  $("#loader-wrapper").hide();
}

var gNodeId = 0;
var emptyLabel = " ";
var defaultLabel = emptyLabel;
var defaultNodeColor = "white";
var defualtEdgeProperties = undefined; // not working for some reason...



var test_quest = {
"id" : "quest_001",

"screens" : [
{"id" : 'screen1',  pic : "http://orig11.deviantart.net/ebd1/f/2007/286/3/b/cloudy_mountains_v_by_mutrus.jpg", 
"text": "You walk along the windy path... (a test quest)", 
choices: [
   {"text": "You have a cat..." , 	 "eval-text": "HaveItem('cat').use", 		"show-if": "HaveItem('cat')" , 		"click": "alert('MeoooW!');" },  
   {"text": "Look around.. ",		 "eval-text": "",			"show-if": "" ,			"click": "alert('Nothing happens');" },  
   {"text": "Coninue... ",		 "eval-text": "",				"show-if": "true" ,			"click": "Next('screen2');" },
   {"text": "Just test button " ,		 "eval-text": "", 				"show-if": "" , 			"click": "" },
 ]
}

, {"id" : 'screen2',  pic : "https://4.bp.blogspot.com/-_O47SPTwyTg/VbTfhWS65KI/AAAAAAAAdU8/A3ozQNa7VCY/s1600/viata-si-fantezie-1_167e9fed2275c6.jpg", 
"text": "You see a mysterious tree (here for testing purposes)...", 
choices: [
   {"text": "You have a cat... really." , 	 "eval-text": "HaveItem('cat').use", 		"show-if": "HaveItem('cat')" , 			"click": "alert('MeoooW!');" },  
   {"text": "Get back.." , 		 "eval-text": "", 				"show-if": "", 				"click": "Next('screen1');" },
   {"text": "See ya (End quest)",	 "eval-text": " '!'+'?' ",			"show-if": "" ,				"click": "EndQuest();" }
 ]
}


]
};




function EndQuest() {
	$.featherlight.current().close();
}

var quest_index = 0; // temp index to get quests from the list

function addClick(node) {
 shape = node.shape;
 shape.items.forEach((item) => {
    item.set = shape;
    item.click( function(x , y , e) {
	// TODO: add quest_id to node. Then load quest id html via ajax or from the page, smth like "quest_123.html .jQuery-Selector"
	// or call generic procedure to generate a DOM for quest
	var quest = test_quest;
	if (node.id == "start")
		quest = QuestStart;
	else if (quest_index < Quests.length)
		quest = Quests[quest_index++];
	$.featherlight( QuestCreateDOM(quest) );
    });
 });

}

function render (canvas , node) {
     var color = defaultNodeColor;
     if (node.color) 
	color = node.color;
    
     node.shape = canvas.set()
     node.shape
     .push(canvas.ellipse(0, 0, 30, 20)
        .attr({ stroke: color, 'stroke-width': 2, fill: color, 'fill-opacity': 0 }));
  
   if (node.label && emptyLabel != node.label) {
     node.shape.push(canvas.rect(-30 , 20 , 60 , 22).attr({ "class": "background", fill: "white" }))
     node.shape.push(canvas.text(0, 30, node.label || node.id))
   }
    
   node.shape.translate(node.point[0], node.point[1]);

   addClick(node);

   return node.shape;
}

function generateGraph (g , startNode, endNode , avg_branching, avg_length, overall_nodes) {
	
	branching = avg_branching;
	// create long path
	if (overall_nodes <= avg_length) {
		var prev_node = startNode;
		for (var i = 0 ; i < avg_length; i++) {
			var cur_node = "id"+gNodeId.toString();
			g.addNode(cur_node , {label: defaultLabel , render: render});
			gNodeId++;
			g.addEdge(cur_node , prev_node , defualtEdgeProperties);
			prev_node = cur_node;
		}
		g.addEdge(prev_node , endNode);
		return;
	}
	// create branching
	for (var i = 0 ; i < branching; i++) {
		var start_node = "id"+gNodeId.toString();
		gNodeId++;
		g.addNode(start_node , {label: defaultLabel  , render: render} );
		g.addEdge(start_node , startNode , defualtEdgeProperties);

		var end_node = "id"+gNodeId.toString();
		gNodeId++;
		g.addNode(end_node , {label: defaultLabel , render: render });
		g.addEdge(end_node , endNode , defualtEdgeProperties );

		generateGraph(g, start_node , end_node, avg_branching , avg_length , overall_nodes - branching );
                }
}

// adds item to inventory
function ShowItemInventory(item) {
	// adds featherlight with item
	lightbox_id="image"+item.id;
      	img_src = item.pic;
      	placeholder = $("#inventory").append('<div class="img_href"><a href="'+img_src+'"><div class="img_thumbnail"><img class="inventory_img" id="'+lightbox_id+'" src="'+img_src+'"></img></div></a></div>');
	img_link = placeholder.find("a");
	img = img_link.find( "img" );
	img.load(function() {
                	//$(this).css('max-width', cards_source[j].columns*100+"%"); 
	                pic_height =  Math.floor($(this).parent().width() * 1.4);
                	$(this).parent().css('max-height', pic_height+'px');
		$(this).parent().parent().append("<div class='inventory_label'>" + item.label + "</div>");	
	  });
	
	// TODO: generate DOM with both img_src and description
	img_link.featherlight(img_src, {type:{image: true}, closeOnClick:   'anywhere' , variant: 'lightbox_full_img' });
}

function createGraph() {

	
	
	startNodeProp = {label: "Start",  css: "node_start"  , render: render , color: "red" };
	endNodeProp = {label: "End" , css: "node_end"  , render: render , color: "blue"};
	
	var g = new Dracula.Graph();

	var avg_branching = QuestMapGraph["branching"] || 2 ;
                var avg_length = QuestMapGraph["length"] || 0;
                var overall_nodes = QuestMapGraph["nodes"] || 4;

	// generate a graph between two nodes
	start = g.addNode("start" , startNodeProp);
	end = g.addNode("end" , endNodeProp);
	generateGraph(g, start , end , avg_branching , avg_length , overall_nodes);
	
 
	var layouter = new Dracula.Layout.Spring(g);
	layouter.layout();
	start.layoutPosX = g.layoutMinX+1;
	start.layoutPosY = g.layoutMaxY-1;
	end.layoutPosX = g.layoutMaxX-2;
	end.layoutPosY = g.layoutMinY+1;

	var width = $("#map_graph").width();
	var height = $("#map_graph").height();
 
	var renderer = new Dracula.Renderer.Raphael('#map_graph', g, width , height);
	renderer.draw();

	// TODO: mark passed ones
	//defaultNodeColor = "blue";
	//renderer.draw();
}

// applies game parms from config urls given
function applyLevel(level) {

  var level_quest = "levels/"+level+"/"+level+"_quests.js"
  var level_style = "levels/"+level+"/"+level+"_style.css"

  // add css and quest
  loadCSS(level_style);
  var script = document.createElement('script');
  script.src = level_quest;
  script.onload = function () {
    //do stuff with the script
    if (!Quests) {
	alert ("Cannot load quests for the level! " + level);
     }
     
    createGraph();

     // finish init
     $("#map").imagesLoaded( function() {
	       endLoading();
	 });    

  };
 document.head.appendChild(script);
 
}



$( function() {
    
	// load level details
	var level = getParameterByName("level");
	if (!level)	{
		level = "garden";
	}
	applyLevel(level);

	ShowItemCallback = ShowItemInventory;

});