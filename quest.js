




/** The quest engine generate DOM objects with HTML like this using (JSON) quest description

<!---------- SCREEN 1 -------------------->
<div class="quest_screen" id="screen1">

<div class="quest_picture" id="screen1_pic">
<img src="http://orig11.deviantart.net/ebd1/f/2007/286/3/b/cloudy_mountains_v_by_mutrus.jpg"> </img>
</div> 

<div class="quest_text" id="screen1_text">
...
</div>

<div class="quest_choices" id="screen1_choices">
   <div class="quest_choice" id="screen1_choice1" quest-behaviour="">TEXT</div>
   <div class="quest_choice" id="screen2_choice2" quest-behaviour="">TEXT</div>
   <div class="quest_choice" id="screen3_choice3" quest-behaviour="">TEXT</div>
</div>

</div>

*/




// available functions:
// 1. MaxBonus(challange) - finds an item with max bonus. Returns null_item if nothing found
// e.g. MaxBonus("T").bonus > 20
// 2. HaveItem(id) - checks item by item id
// 3. Next(screen_id) - goes to the screen
// 4. AddItem(id) - adds item by id



function QuestOpenScreen( next_screen_id ) {
	$(".quest_screen").hide();
	$("#"+next_screen_id).show();
}

var Next = QuestOpenScreen;


function QuestCreateDOM ( quest ) {
		
	var screen_index = 0;
	quest_class = "quest";
	if (quest["class"]) 
		quest_class = quest["class"];
	
	dom = jQuery( '<div/>', { id: quest.id , class:"quest"} );
	
	screens = quest.screens;
	for (var i = 0 ; i < screens.length ; i++)	{
		screen = screens[i];

		screen_class = "quest_screen";
		screendom = jQuery('<div/>', {   id: screens[i].id,   class: screen_class});
		if (screens[i]["class"]) 
			screendom.addClass(quest["class"]);
		screendom.appendTo(dom);

		pic_class = "quest_picture";
		pic_block_class = "quest_picture_block";
		pic = '<div class="'+pic_block_class+'" id="'+screens[i].id+'_pic"> <img class="'+pic_class+'" src="'+screens[i].pic+'"> </img></div>';
		screendom.append(pic);
		
		text_class = "quest_text"; // TODO: make customizable
		text =  jQuery('<div/>', {   id: screens[i].id+'_text',   class: text_class ,   html: screens[i].text});
		text.appendTo(screendom);

		choices_class = "quest_choices"; // TODO: make customizable
		choices =  jQuery('<div/>', {   id: screens[i].id+'_choices',   class: choices_class });
		choices.appendTo(screendom);
		
		for (var j = 0 ; j < screen.choices.length; j++) {
			var choice = screen.choices[j];
			var show_if = "1";	
			if (choice["show-if"])
				show_if = eval (choice["show-if"]);
			var click = "return false;"
			if (choice["click"])
				click = choice["click"];

			// Skip. NOTE: dialog screens are not reevaluated on every screen shown. maybe it worth adding a feature to reeval 'show-if' on every dialog show
			if (!show_if) continue;
			
			var eval_text ="";
			if (choice["eval-text"])
				 eval_text = eval (choice["eval-text"]);

			choice_class = "quest_choice"; // TODO: make customizable
			choice =  jQuery('<div/>', {   id: screens[i].id+'_choice_'+j.toString(),   class: choice_class , text: choice.text+eval_text });
			choice.attr('onClick', click);
			// TODO: impl function to open next quest screens
			choice.appendTo(choices);
		}
		
		// don't show next screens until needed
		if ( i != 0 )
			screendom.hide(); 
		
	}
	return dom;
}