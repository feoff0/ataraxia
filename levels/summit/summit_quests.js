// TODO: this is a adult-life start level
// surroundings are more or less welcoming and ready to support you a bit but there are little of gifts, some work and negotiations ahead

var start_quest = {
"id" : "quest_start",

"screens" : [
{"id" : 'screen1',  pic : "http://wallpaperswide.com/download/under_the_tree-wallpaper-1280x720.jpg", 
"text": "You lost on your trail and unsure where to proceed. You pass through the vale filled with sun to the old oak standing on the edge of green forest.<br/> Suddenly a tree bark starts to move, and assembles in a pattern of a kind old woman, she stares at you and smiling kindly...", 
choices: [
   {"text": "Hello?..." , 	 "eval-text": "", 		"show-if": "" , 		"click": "Next('screen2')" },  
 ]
}

, {"id" : 'screen2',  pic : "https://s-media-cache-ak0.pinimg.com/originals/f0/37/f5/f037f5cfd27bbc87f5e9ab4f9f033c30.jpg", 
"text": "-Welcome to my land -, she says, - Don't be afraid. I'm the Granny, the spirit of the land, my soil gives birth to any tree and creature in these woods.<br/>I suspect you seek a pool to tranquil your soul after your hardships and travels.<br/>As I love any creature of the forest, I'm ready to share my love with you.<br/>I do feel you a strong and capable person, you've been to many places outside my forest.<br/>I wonder, why are your so strong to come here.<br/>Say, is yourself at first compassionate.. or accountable.. or versatile..  or discerning?", 
choices: [
   {"text": "Compassionate" , 	 "eval-text": "", 		"show-if": "" ,	 		"click": "Next('unicorn');" },  
   {"text": "Accountable",	 "eval-text": "",		"show-if": "" ,			"click": "Next('bear');" },
   {"text": "Descerning",	 "eval-text": "",		"show-if": "" ,			"click": "Next('owl');" },
   {"text": "Versatile",	 "eval-text": "",		"show-if": "" ,			"click": "Next('cat')" },
 ]
}

, {"id" : 'unicorn',  pic : "http://orig15.deviantart.net/f60c/f/2014/068/8/0/patronus_by_penny_dragon-d79m9fn.jpg", 
"text": "A glowing ghostly creature appears near of you. The Granny whispers: <br/>-A unicorn. Honest and altruistic mystical creature. It values kind and ethical beings. It's courageous but trusty.<br/>The Granny continues: <br/>- It may be your guide. Do you feel a bond with the creature, will you rely on it?", 
choices: [
   {"text": "Yes" ,	 	 "eval-text": "", 		"show-if": "" ,	 		"click": "AddItem('unicorn'); Next('screen3');" },  
   {"text": "No",		 "eval-text": "",		"show-if": "" ,			"click": "Next('screen2');" }
 ]
}

, {"id" : 'bear',  pic : "http://www.doloresbaker.com/uploads/3/4/9/4/34947287/4503668.jpg", 
"text": "A glowing ghostly creature appears near of you. The Lady whispers: <br/>-A bear. Careful and serious creature. It values dutiful and reliable beings. It cares for supplies and security to feed his cubs and survive harsh winters. <br/> The Granny continues:<br/>- It may be your guide. Do you feel a bond with the creature, will you rely on it?", 
choices: [
   {"text": "Yes" ,	 	 "eval-text": "", 		"show-if": "" ,	 		"click": "AddItem('bear'); Next('screen3');" },  
   {"text": "No",		 "eval-text": "",		"show-if": "" ,			"click": "Next('screen2');" }
 ]
}

, {"id" : 'owl',  pic : "http://www.theplotbunny.com/wp-content/uploads/2016/12/owl-1024x717.jpg", 
"text": "A glowing ghostly creature appears near of you. The Granny whispers: <br/>-An owl. Reasonable and autonomous creature. It values competentness and knowledge. Albeit it's wisdom has some twists - only a mindful person will learn which of his advices to follow.<br/> The Granny continues:<br/>- It may be your guide. Do you feel a bond with the creature, will you rely on it?", 
choices: [
   {"text": "Yes" ,	 	 "eval-text": "", 		"show-if": "" ,	 		"click": "AddItem('owl'); Next('screen3');" },  
   {"text": "No",		 "eval-text": "",		"show-if": "" ,			"click": "Next('screen2');" }
 ]
}

, {"id" : 'cat',  pic : "https://ih1.redbubble.net/image.4239180.4675/flat,1000x1000,075,f.jpg", 
"text": "A glowing ghostly creature appears near of you. The Granny whispers: <br/>-A wild cat. Agile and spontaneous creature. It values action, courage, skill and luck. Cat likes to play and cares of its own pleasure.<br/> The Granny continues:<br/>- It may be your guide. Do you feel a bond with the creature, will you rely on it?", 
choices: [
   {"text": "Yes" ,	 	 "eval-text": "", 		"show-if": "" ,	 		"click": "AddItem('cat'); Next('screen3');" },  
   {"text": "No",		 "eval-text": "",		"show-if": "" ,			"click": "Next('screen2');" }
 ]
}

, {"id" : 'screen3',  pic : "https://68.media.tumblr.com/4959c20e7233dc637ac515db2876cc47/tumblr_ns9yj576fJ1ta3cc1o1_500.gif",  // TODO: add something about what to expect on this level
"text": "The Granny smiles:<br/>-Let the spirit guide you to the mountain summit. A place where the Wind whispers. You will find answers you seek in its words.<br/>Suddenly the face disappears. You see just an ordinary oak. The ghostly creature disappears too, but you continue feel its presence.", 
choices: [ 
   {"text": "Continue..." , 	 "eval-text": "", 		"show-if": "" ,	 		"click": "Next('screen4');" }
 ]
}

, {"id" : 'screen4',  pic : "http://www.forestwander.com/wp-content/original/2009_01/forest-trail-trees.JPG", 
"text": "You notice a trail going deeper into the forest.<br/> The trail has some twists but you know all of them will eventually lead you to the summit. <br/> (Choose your next destination on the map)", 
choices: [
   {"text": "Continue..." , 	 "eval-text": "", 		"show-if": "" ,	 		"click": "EndQuest();" }
 ]
}


]
};


var bee_quest = {
"id" : "quest_bees",

"screens" : [
{"id" : 'screen1',  pic : "http://megabee.com/images/hiveinorchard.jpg", 
"text": "You approach a man-made structure, you recognize a beehive. No signes of bees tough.", 
choices: [
   {"text": "Continue..." , 	 "eval-text": "", 		"show-if": "" , 		"click": "Next('screen2')" },  
 ]
}

, {"id" : 'screen2',  pic : "https://s-media-cache-ak0.pinimg.com/736x/67/7a/72/677a72898cf6333b9fe428d4a33b0f04.jpg", 
"text": "Suddenly a giant human-size bee approaches from the trees growing nearby.<br/>It wears a hat and holds a retro-style business bag case in its arm-like insect leg.<br/>- Greetings! Don't fear me, I humbly ask, - says the bee-person, - Let me introduce myself. My name is Beep. What is your name?<br/> I'm a travelling salesperson. Mostly often I fly around these vales offering delightful honey of my auntie's production. She has the largest and most respected hive in these mountains.<br/>Granny mentioned you'll be passing by so I was in a hurry to welcome you from our hive and present a small gift<br/>Beep, the bee salesperson, opens his case and hands you a small jar filled with honey.", 
choices: [
   {"text": "Take the honey jar..." ,	 "eval-text": "", 		"show-if": "" ,	 		"click": "AddItem('honey'); Next('screen3');" },  
   {"text": "Refuse..." , 	 "eval-text": "", 		"show-if": "" ,	 		"click": "Next('screen3');" },  
 ]
}

, {"id" : 'screen3',  pic : "http://www.giant-mountains.info/data/akce/1169/magical-honey-tuesdaysin-wellness-hotel-svornost-in-harrachov.jpg", 
"text": "Beep continues:<br/>-I'd like to inform you I have a special offering from my auntie! Buy our magical cosmetic honey with 30% discount. It's great for your bodycare and skin! A neverneding youth with our honey!<br/>-We accept no cards for payment, flowers only!<br/>Beep shows you a jar of cosmetic honey.", 
choices: [
   {"text": "You hear a Bear Spirit advice 1 flower is a fair price for the honey in these mountains. Negotiate..." , 	 "eval-text": "", 		"show-if": "HaveItem('bear')&&HaveItem('flower');" ,	 		"click": "AddItem('cosmetic_honey');Next('screen4');" },
   {"text": "You hear a Bear Spirit advice to say you a trustworthy and reliable person. You buy a honey in credit..." , 	 "eval-text": "", 		"show-if": "HaveItem('bear') && !HaveItem('flower');" ,	 		"click": "Next('screen4');" }, // TODO: add a screen with credit description ]
   {"text": "Appeal to Beep you have only 1 flower but it's the best flower you have (60% chance)" ,			 "eval-text": "", 		"show-if": "HaveItem('flower');" ,	 			"click": "AddItem('cosmetic_honey'); Next('screen4');" }, // TODO: add a random dice roll
   {"text": "Appeal to Beep to give you a free sample of magical honey (20% chance)" ,			 	"eval-text": "", 		"show-if": "" ,	 					"click": "AddItem('cosmetic_honey'); Next('screen4');" },
   {"text": "Decline the offer." ,			 							"eval-text": "", 		"show-if": "" ,	 					"click": "Next('screen4');" } 
 ]
}

, {"id" : 'screen4',  pic : "https://mcuoneclipse.files.wordpress.com/2012/08/wild-bees.png", 
"text": "Beep's best wishes. He leaves you saying he is in a hurry getting to his next valued customer. You notice a small bee on the flower getting the nectar from a violet flower. Soon it flies away. It's time to continue your journey.", 
choices: [
   {"text": "Continue..." , 	 "eval-text": "", 		"show-if": "" ,	 		"click": "EndQuest();" }
 ]
}


]
};



var bee_rock = {
"id" : "quest_rocks",

"screens" : [
{"id" : 'screen1',  pic : "https://steamuserimages-a.akamaihd.net/ugc/25087975291817161/4B01A1274CA990A3577CC37E1DAA72F88DE353F8/", 
"text": "You see great mountain range ahead. Cold fresh air fills your chest.", 
choices: [
   {"text": "Continue..." , 	 "eval-text": "", 		"show-if": "" , 		"click": "Next('screen2')" },  
 ]
}

, {"id" : 'screen2',  pic : "http://www.megalithic.co.uk/a558/a312/gallery/Eastern_Europe/lapland09.jpg", 
"text": "You see some rocks that shaped like a living creature, a bird. It will take some time to climb up to the rocks. You hear a strange sound coming from the place.", 
choices: [
   {"text": "Climb up to ..." ,	 "eval-text": "", 		"show-if": "" ,	 		"click": "Next('screen3');" },  
   {"text": "Ignore the strange stone..." , 	 "eval-text": "", 		"show-if": "" ,	 		"click": "Next('screenend');" },  
 ]
}

// TODO: complete the screen below

, {"id" : 'screen3',  pic : "http://www.driftcreek.org/wp-content/uploads/2014/02/American_Robin_Nest_with_Eggs.jpg", 
"text": "Behind the rock you find a small construction of a dry grass - a nest. You see blue eggs in it. It seems like a sound coming from the place was produced by these egg parents.", 
choices: [
   {"text": "You hear an owl's advice. Don't touch these eggs as your smell will bother eggs' parents." ,	 	 "eval-text": "", 		"show-if": "HaveItem('owl');" ,	 		"click": "AddItem('cosmetic_honey');Next('screen4');" },
   {"text": "You hear a cat's advice. These eggs looks very nice. I bet one or two might be of use" , 			 "eval-text": "", 		"show-if": "HaveItem('cat');" ,	 		"click": "Next('screen4');" }, // TODO: add a screen with credit description ]
   {"text": "Take one egg" ,										 "eval-text": "", 		"show-if": "" ,	 			"click": "AddItem('blue_egg'); Next('screen4');" }, // TODO: add a random dice roll
   {"text": "Appeal to Beep to give you a free sample of magical honey (20% chance)" ,			 	"eval-text": "", 		"show-if": "" ,	 					"click": "AddItem('cosmetic_honey'); Next('screen4');" },
   {"text": "Decline the offer." ,			 							"eval-text": "", 		"show-if": "" ,	 					"click": "Next('screen4');" } 
 ]
}

, {"id" : 'screenend',  pic : "http://wdfw.wa.gov/viewing/guides/yakima/graphics/mtbluebird_male.jpg", 
"text": "A small bluebird sits on the nearby tree and watches over as you leave the location.", 
choices: [
   {"text": "Continue..." , 	 "eval-text": "", 		"show-if": "" ,	 		"click": "EndQuest();" }
 ]
}


]
};


var Summit_RandomUniqueQuests = 
[
bee_quest 
];

// vars imported to the main script

var Quests = Summit_RandomUniqueQuests;
var QuestMapGraph = {"branching": 2, "length":0 , "nodes":4};
var QuestStart = start_quest