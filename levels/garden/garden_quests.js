// TODO: this is a adult-life start level
// surroundings are more or less welcoming and ready to support you a bit but there are little of gifts, some work and negotiations ahead

var start_quest = {
"id" : "quest_start",

"screens" : [
{"id" : 'screen1',  pic : "http://www.thinweasel.com/cdn/800x530/english-gardens-extension-master-gardener-30083.jpg", 
"text": "Welcome! Here you start your virtual journey to find emotional peace and inner balance by discovering the magical Pool of Ataraxia. <br/>Your journey begins at the entrance of magical garden. This place is warm and welcoming. It is inhabited by kind magical creatures, and the Warden who lives here in solitude providing guidence to travellers seeking for answers, peace and happiness.<br/> The man is known to have a map to the Pool.", 
choices: [
   {"text": "Proceed" , 	 "eval-text": "", 		"show-if": "" , 		"click": "Next('screen2_tutorial')" }
 ]
}

, {"id" : 'screen2_tutorial',  pic : "levels/garden/tutorial1.jpg", 
"text": "There are several places (levels) in the game. You choose your way on a road from the Start location to the End location of the game level. <br/>To move, click a location (white circle). You visit a location where various exciting events occur.", 
choices: [
   {"text": "Ok, let's start!" , 	 "eval-text": "", 		"show-if": "" ,	 		"click": "EndQuest();" }  
 ]
}

]
};


// mak1
// https://s-media-cache-ak0.pinimg.com/236x/84/92/76/849276b8e646b46e7ed025d259d3fccf.jpg
// https://s-media-cache-ak0.pinimg.com/736x/e4/5e/3e/e45e3eefb2a11f92f2f98617191f760a.jpg
// https://s-media-cache-ak0.pinimg.com/736x/c5/3b/fa/c53bfa2d4443eed919d15b089015197f.jpg
// 
// 
// mak2
// http://saimg-a.akamaihd.net/saatchi/284005/art/1741929/910228-7.jpg
// https://s-media-cache-ak0.pinimg.com/originals/84/df/a5/84dfa58ce82f517df75281cf7b6850bb.jpg
// http://favim.com/orig/201104/22/Favim.com-21852.jpg
// http://cdn.playbuzz.com/cdn/04262de6-de92-44fa-8f66-7f632c9193ac/37e1239b-37f2-43bb-9d30-41e0bd5b95b1.jpg
// http://poetry.wrr.ng/wp-content/uploads/2015/05/Nigerian-Poetry-UNTOLD-CHILDHOOD-by-Prince-Kuti-Adetoye-Olayinka-1024x640.jpg
// https://s-media-cache-ak0.pinimg.com/736x/5e/75/7f/5e757fc457a7e5c2dba17629e20ba427.jpg
// http://c300221.r21.cf1.rackcdn.com/moments-of-childhood-3-1354558833_b.jpg
// https://pbs.twimg.com/media/C9LfJPOXsAAjIVy.jpg


var cats_quest = {
"id" : "quest_cats",

"screens" : [
{"id" : 'screen1',  pic : "levels/garden/kittens.jpg", 
"text": "You see a huge basket on your way. There are several kittens playing.<br/>Suddenly, a white kitten jumps from the basket and moves towards you. It starts starts talking human language with a high childish voice.", 
choices: [
   {"text": "Wow..." , 	 "eval-text": "", 		"show-if": "" , 		"click": "Next('screen2')" },  
 ]
}

, {"id" : 'screen2',  pic : "http://www.allmacwallpaper.com/pic/Thumbnails/7134_728.jpg", 
"text": "-Greeetings sirrr. My sibilings are werre currious about you.<br/>But they arre afrraid. And can't speak human.<br/>I'm the special one. My name is Kindercat.<br/> But all of us do love human picturres. Please, missterr show us some pictures frrom the human picturre book. <br/>The kitten leans toward a small tome. It seems to heavy for a kitten to lift.<br/> - We will show you a way to the Warrrden if we trrust and no morre afrraid of you, sirr.", 
choices: [
   {"text": "Take the book and open it..." ,	 "eval-text": "", 		"show-if": "" ,	 "click": "Next('screen3');" },  
   {"text": "Refuse..." , 	 "eval-text": "", 		"show-if": "" ,	 		 "click": "AddItem('sad_kitten'); Next('screen_end');" },  
 ]
}

// TODO: add items
, {"id" : 'screen3',  pic : "levels/garden/mak1.png", 
"text": "The book has rather pictures drawn in a very different style.<br/> A wonder why kittens love the books (not to mention the fact fact one of them is talking).<br/>- Please sirrr, tell us, what picture describes your childhood.. when you were a little kitten... I mean kid.<br/>Kittens stare at you with curiosity.", 
choices: [
   {"text": "1" , 			 "eval-text": "", 		"show-if": "" ,	 		"click": "AddItem('mak1_1'); Next('screen4');" },
   {"text": "2" , 	 		"eval-text": "", 		"show-if": "" ,	 		"click": "AddItem('mak1_2'); Next('screen4');" }, 
   {"text": "3" ,			 "eval-text": "", 		"show-if": "" ,			"click": "AddItem('mak1_3'); Next('screen4');" }, 
   {"text": "4" ,			"eval-text": "", 		"show-if": "" ,			"click": "AddItem('mak1_4'); Next('screen4');" }
 ]
}

// TODO: add screens
, {"id" : 'screen4',  pic : "", 
"text": "You turn the page.<br/> - Please sirrr, tell us, what happened next, when you grew up a bit...", 
choices: [
   {"text": "1" , 			 "eval-text": "", 		"show-if": "" ,	 		"click": "AddItem('mak2_1'); Next('screen5');" },
   {"text": "2" , 	 		"eval-text": "", 		"show-if": "" ,	 		"click": "AddItem('mak2_2'); Next('screen5');" }, 
   {"text": "3" ,			 "eval-text": "", 		"show-if": "" ,			"click": "AddItem('mak2_3'); Next('screen5');" }, 
   {"text": "4" ,			"eval-text": "", 		"show-if": "" ,			"click": "AddItem('mak2_4'); Next('screen5');" },
   {"text": "5" , 			 "eval-text": "", 		"show-if": "" ,	 		"click": "AddItem('mak2_5'); Next('screen5');" },
   {"text": "6" , 	 		"eval-text": "", 		"show-if": "" ,	 		"click": "AddItem('mak2_6'); Next('screen5');" }, 
   {"text": "7" ,			 "eval-text": "", 		"show-if": "" ,			"click": "AddItem('mak2_7'); Next('screen5');" }, 
   {"text": "8" ,			"eval-text": "", 		"show-if": "" ,			"click": "AddItem('mak2_8'); Next('screen5');" }
  ]
}

, {"id" : 'screen_end',  pic : "https://mcuoneclipse.files.wordpress.com/2012/08/wild-bees.png", 
"text": "Beep's best wishes. He leaves you saying he is in a hurry getting to his next valued customer. You notice a small bee on the flower getting the nectar from a violet flower. Soon it flies away. It's time to continue your journey.", 
choices: [
   {"text": "Continue..." , 	 "eval-text": "", 		"show-if": "" ,	 		"click": "EndQuest();" }
 ]
}


]
};



var bee_rock = {
"id" : "quest_rocks",

"screens" : [
{"id" : 'screen1',  pic : "https://steamuserimages-a.akamaihd.net/ugc/25087975291817161/4B01A1274CA990A3577CC37E1DAA72F88DE353F8/", 
"text": "You see great mountain range ahead. Cold fresh air fills your chest.", 
choices: [
   {"text": "Continue..." , 	 "eval-text": "", 		"show-if": "" , 		"click": "Next('screen2')" },  
 ]
}

, {"id" : 'screen2',  pic : "http://www.megalithic.co.uk/a558/a312/gallery/Eastern_Europe/lapland09.jpg", 
"text": "You see some rocks that shaped like a living creature, a bird. It will take some time to climb up to the rocks. You hear a strange sound coming from the place.", 
choices: [
   {"text": "Climb up to ..." ,	 "eval-text": "", 		"show-if": "" ,	 		"click": "Next('screen3');" },  
   {"text": "Ignore the strange stone..." , 	 "eval-text": "", 		"show-if": "" ,	 		"click": "Next('screenend');" },  
 ]
}

// TODO: complete the screen below

, {"id" : 'screen3',  pic : "http://www.driftcreek.org/wp-content/uploads/2014/02/American_Robin_Nest_with_Eggs.jpg", 
"text": "Behind the rock you find a small construction of a dry grass - a nest. You see blue eggs in it. It seems like a sound coming from the place was produced by these egg parents.", 
choices: [
   {"text": "You hear an owl's advice. Don't touch these eggs as your smell will bother eggs' parents." ,	 	 "eval-text": "", 		"show-if": "HaveItem('owl');" ,	 		"click": "AddItem('cosmetic_honey');Next('screen4');" },
   {"text": "You hear a cat's advice. These eggs looks very nice. I bet one or two might be of use" , 			 "eval-text": "", 		"show-if": "HaveItem('cat');" ,	 		"click": "Next('screen4');" }, // TODO: add a screen with credit description ]
   {"text": "Take one egg" ,										 "eval-text": "", 		"show-if": "" ,	 			"click": "AddItem('blue_egg'); Next('screen4');" }, // TODO: add a random dice roll
   {"text": "Appeal to Beep to give you a free sample of magical honey (20% chance)" ,			 	"eval-text": "", 		"show-if": "" ,	 					"click": "AddItem('cosmetic_honey'); Next('screen4');" },
   {"text": "Decline the offer." ,			 							"eval-text": "", 		"show-if": "" ,	 					"click": "Next('screen4');" } 
 ]
}

, {"id" : 'screenend',  pic : "http://wdfw.wa.gov/viewing/guides/yakima/graphics/mtbluebird_male.jpg", 
"text": "A small bluebird sits on the nearby tree and watches over as you leave the location.", 
choices: [
   {"text": "Continue..." , 	 "eval-text": "", 		"show-if": "" ,	 		"click": "EndQuest();" }
 ]
}


]
};



var Garden_RandomUniqueQuests = 
[ cats_quest
];

var Quests = Garden_RandomUniqueQuests;
var QuestMapGraph = {"branching": 1, "length":2 , "nodes":2};
var QuestStart = start_quest

